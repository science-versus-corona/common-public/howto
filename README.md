# GIT - short instructions to start

Our Git repositories can be found [**here**](https://gitlab.com/science-versus-corona)

## Access

Request access to our GitLab in channel **#svc-git-access** on Slack, state your e-mail address and the project/group 
you would like access to. The standard role is Developer, kindly motivate when you'd like a different access level.

## Tools to download

You need to download Git. Additionally, you could use a GUI for it like GitKraken.

### Download Git

Click [here](https://git-scm.com/downloads) to download Git

### (optional) Download GitKraken or another Git UI tool

Click [here (Linux, Mac & Windows)](https://www.gitkraken.com/download) to download GitKraken, the best to my (Mark's) 
opinion but not all functionality is available in the free version. Alternatives are 
[SourceTree (Mac & Windows)](https://www.sourcetreeapp.com/) which is buggy but free or [TortoiseGit (Windows)]
(https://tortoisegit.org/). There are more options out there.

## Setup connection to the remote repositories

### Generate an SSH keypair

* On [GitHub](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
you'll find a tutorial to generate a keypair if you do not have that yet. **Do not** follow the last step and add it 
to GitHub since we are using GitLab.
* [Create a GitLab account](https://gitlab.com/users/sign_up) if you did not do that yet. Preferably use the same email 
as used within the Science versus Corona project/on Slack.
* Add the public key (the content of the file with the pub extension, one of two files you just created) to GitLab [here](https://gitlab.com/profile/keys).

### Clone the repository you need

If you [**browse**](https://gitlab.com/science-versus-corona) to the repository you use, there's a clone button in the upper right corner of the page. Press it, copy the 
ssh link. Then in your (git) bash, type <code>git clone git@gitlab.com:science-versus-corona/common-public/howto.git</code> 
as an example if you would like to clone the howto repository. Or use the uri in your Git GUI of choice.

If this worked you can now push and pull to your Git repository!

## (optional) Git Tutorial

[Here](https://www.katacoda.com/courses/git) I've found a nice Git tutorial. But there's loads of them on the internet. 
Good luck, have fun defeating Corona!

## Need help?

If you have problems or questions using Git after trying a tutorial, contact Mark Muizer over slack or mark@muizer.net .
Or ask a team member :).

